# AtCoder

def taskA():
    # Practice Task A - Welcome to AtCoder
    a = int(input(""))
    b, c = map(int, input("").split())
    s = str(input(""))

    print("\n{sum} {text}".format(sum=a+b+c, text=s))


class taskB:
    # Practice Task B - Interactive Sorting
    def __init__(self):
        self.N, self.Q = map(int, input("").split())
        self.items = [chr(65+i) for i in range(self.N)]
        self.ans = self.items.copy()

    def check_item(self, start_indx, item_count, count=0):
        for i in range(start_indx, item_count-1):
            for j in range(i, item_count):
                if self.items[i] == self.ans[j]: continue
                count += 1
                if count > self.Q:
                    return count
                ret = input("? {c1} {c2}\n".format(c1=self.items[i], c2=self.ans[j]))
                ans_indx = self.ans.index(self.items[i])
                if ret == ">":
                    self.ans[ans_indx], self.ans[j] = self.ans[j], self.ans[ans_indx]
                elif ret == "<":
                    self.items = self.ans.copy()
                    # left part
                    count = self.check_item(0, ans_indx, count)
                    self.items = self.ans.copy()
                    # right part
                    count = self.check_item(ans_indx+1, self.N, count)
                    self.items = self.ans.copy()
                    return count
        return count


    def run(self):
        print("Total Question : ", self.check_item(0, self.N))
        print("\n! ", end="")
        for item in self.ans:
            print(item, end="")
        print()

    


if __name__ == "__main__":
    taskA()
    
    task = taskB()
    task.run()